import pickle
import numpy as np

prefix = '../data/CIFAR10/data_batch_'


def unpickle(file):
    import cPickle
    with open(file, 'rb') as fo:
        dict = cPickle.load(fo)
    return dict


def one_hot(index, num_classes):
    assert index < num_classes and index >= 0
    tmp = np.zeros(num_classes, dtype=np.float32)
    tmp[index] = 1.0
    return tmp


def GetCIFAR():
    train_img = np.zeros([10000, 32, 32, 3])
    train_label = np.zeros([10000, 10])
    for batch in range(1, 6):
        d = unpickle(prefix + str(batch))
        data = d[b'data']
        label = np.asarray(d[b'labels'])
        data = np.reshape(data, [-1, 3, 32, 32])
        data = data.swapaxes(1, 3).swapaxes(1, 2)
        label = np.reshape(label, [-1, 1])
        label_one_hot = np.zeros([10000, 10])
        for i in range(label.shape[0]):
            label_one_hot[i] = one_hot(label[i][0], 10)

        if batch == 1:
            train_img = data
            train_label = label_one_hot
        else:
            train_img = np.concatenate((train_img, data), axis=0)
            train_label = np.concatenate((train_label, label_one_hot), axis=0)
    print(train_img.shape)
    print(train_label.shape)

    d = unpickle('../data/CIFAR10/test_batch')
    data = d[b'data']
    label = np.asarray(d[b'labels'])
    data = np.reshape(data, [-1, 3, 32, 32])
    data = data.swapaxes(1, 3).swapaxes(1, 2)
    label = np.reshape(label, [-1, 1])
    label_one_hot = np.zeros([10000, 10])
    for i in range(label.shape[0]):
        label_one_hot[i] = one_hot(label[i][0], 10)

    test_img = data
    test_label = label_one_hot

    print(test_img.shape)
    print(test_label.shape)



    train_data = []
    for i in range(train_img.shape[0]):
        train_data.append((train_img[i]/255.0, train_label[i]))

    test_data = []
    for i in range(test_img.shape[0]):
        test_data.append((test_img[i]/255.0, test_label[i]))

    return train_data, test_data


