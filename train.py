import resnet
import read_cifar

train, valid = read_cifar.GetCIFAR()


n = 3
resnet20 = resnet.ResNet_v2(train_data=train, valid_data=valid, test_data=None, weight_decay=1e-4,
                            list_rep=[1 + 2*n, 2*n, 2*n],
                            list_filter=[16, 32, 64],
                            reduce_lnr=[82, 123],
                            test_during_train=False,
                            current_save_folder='./save/ResNet20_1/',
                            valid_save_folder='./save/ResNet20_1/valid',
                            logs_folder='./summary/Resnet20_1')

resnet20.train(num_epoch=164, batch_size=128)
