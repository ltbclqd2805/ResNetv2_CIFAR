from __future__ import print_function
import tensorflow as tf
import os
import time
from datetime import timedelta
import numpy as np
import Resnet_input


class ResNet_v2:
    def __init__(self, train_data, valid_data=None, test_data=None, input_size=32, num_channels=3, num_classes=10, list_rep=None,
                 list_filter=None, weight_decay=1e-4, current_save_folder='./save/current/',
                 valid_save_folder='./save/best_valid/', last_save_folder='./save/last/', logs_folder='./summary/',
                 reduce_lnr=None, init_lnr=0.1, test_during_train=True):
        self.train_data = train_data
        self.valid_data = valid_data
        self.test_data = test_data
        self.input_size = input_size
        self.num_channels = num_channels
        self.num_classes = num_classes
        self.list_rep = list_rep
        self.list_filter = list_filter
        self.weight_decay = weight_decay
        self.current_save_path = current_save_folder
        self.last_save_path = last_save_folder
        self.valid_save_path = valid_save_folder
        self.logs_folder = logs_folder
        self.reduce_lnr = reduce_lnr
        self.init_lnr = init_lnr
        self.test_during_train = test_during_train
        self.global_step = tf.contrib.framework.get_or_create_global_step()

        self._input()
        self.inference()
        self.losses()
        self.train_step()
        self.count_trainable_params()
        self.init_session()

    def count_trainable_params(self):
        total_parameters = 0
        for variable in tf.trainable_variables():
            shape = variable.get_shape()
            variable_parametes = 1
            for dim in shape:
                variable_parametes *= dim.value
            total_parameters += variable_parametes
        print("Total training params: %.1fM" % (total_parameters / 1e6))

    def _input(self):
        self.x = tf.placeholder(tf.float32, [None, self.input_size, self.input_size, self.num_channels])
        self.y_ = tf.placeholder(tf.float32, [None, self.num_classes])
        self.is_training = tf.placeholder(tf.bool)

    def conv2d(self, x, filter_size, out_filters, strides=1, padding='VALID', scope=None):
        with tf.variable_scope(scope):
            in_filters = int(x.get_shape()[-1])
            filter = tf.get_variable('DW', [filter_size, filter_size, in_filters, out_filters], tf.float32,
                                     tf.contrib.layers.variance_scaling_initializer())
            return tf.nn.conv2d(x, filter, [1, strides, strides, 1], padding)

    def avg_pool(self, x, filter_size, strides):
        return tf.nn.avg_pool(x, [1, filter_size, filter_size, 1], [1, strides, strides, 1], padding='VALID')

    def max_pool(self, x, filter_size, strides):
        return tf.nn.max_pool(x, [1, filter_size, filter_size, 1], [1, strides, strides, 1], padding='SAME')

    def batch_norm(self, x, n_out, phase_train=True, scope='bn'):
        """
        Batch normalization on convolutional maps.
        Args:
            x:           Tensor, 4D BHWD input maps
            n_out:       integer, depth of input maps
            phase_train: boolean tf.Varialbe, true indicates training phase
            scope:       string, variable scope
        Return:
            normed:      batch-normalized maps
        """
        with tf.variable_scope(scope):
            beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
                               name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
                                name='gamma', trainable=True)
            batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')
            ema = tf.train.ExponentialMovingAverage(decay=0.5)

            def mean_var_with_update():
                ema_apply_op = ema.apply([batch_mean, batch_var])
                with tf.control_dependencies([ema_apply_op]):
                    return tf.identity(batch_mean), tf.identity(batch_var)

            mean, var = tf.cond(phase_train,
                                mean_var_with_update,
                                lambda: (ema.average(batch_mean), ema.average(batch_var)))
            normed = tf.nn.batch_normalization(x, mean, var, beta, gamma, 1e-3)
        return normed

    def subsampling_block(self, x, base_depth, stride, name):
        in_filters = int(x.get_shape()[-1])
        with tf.variable_scope(name):
            shortcut = x
            x = self.batch_norm(x, in_filters, self.is_training, scope='bn1')
            pre_act = tf.nn.relu(x)
            x = self.conv2d(pre_act, filter_size=1, out_filters=base_depth, strides=1, padding='VALID', scope='conv1')

            x = self.batch_norm(x, base_depth, self.is_training, scope='bn2')
            x = tf.nn.relu(x)
            x = self.conv2d(x, filter_size=3, out_filters=base_depth, strides=stride, padding='SAME', scope='conv2')

            x = self.batch_norm(x, base_depth, self.is_training, scope='bn3')
            x = tf.nn.relu(x)
            x = self.conv2d(x, filter_size=1, out_filters=4 * base_depth, strides=1, padding='VALID', scope='conv3')

            shortcut = self.conv2d(shortcut, filter_size=1, out_filters=4*base_depth, strides=stride, padding='SAME', scope='conv_shortcut')

            return tf.add(x, shortcut)

    def identity_block(self, x, base_depth, name):
        in_filters = int(x.get_shape()[-1])
        with tf.variable_scope(name):
            shortcut = x
            x = self.batch_norm(x, in_filters, self.is_training, scope='bn1')
            pre_act = tf.nn.relu(x)
            x = self.conv2d(pre_act, filter_size=1, out_filters=base_depth, strides=1, padding='VALID', scope='conv1')

            x = self.batch_norm(x, base_depth, self.is_training, scope='bn2')
            x = tf.nn.relu(x)
            x = self.conv2d(x, filter_size=3, out_filters=base_depth, strides=1, padding='SAME', scope='conv2')

            x = self.batch_norm(x, base_depth, self.is_training, scope='bn3')
            x = tf.nn.relu(x)
            x = self.conv2d(x, filter_size=1, out_filters=4*base_depth, strides=1, padding='VALID', scope='conv3')

            return tf.add(x, shortcut)

    def inference(self):
        with tf.variable_scope('first_conv'):
            output = self.batch_norm(self.x, self.num_channels, self.is_training, scope='first_bn')
            output = tf.nn.relu(output)
            output = self.conv2d(output, filter_size=3, out_filters=self.list_filter[0], strides=1, padding='SAME', scope='conv')
            print('Shape after first conv layer: ', output.get_shape().as_list())

        num_block = len(self.list_rep)
        for block in range(num_block):
            with tf.variable_scope('Block_' + str(block+1)):
                stride = (1 if block == 0 else 2)
                output = self.subsampling_block(output, self.list_filter[block], stride=stride, name='Unit_1')
                for rep in range(self.list_rep[block] - 1):
                    output = self.identity_block(output, self.list_filter[block], name='Unit_' + str(rep+2))
                print('Shape after block ' + str(block + 1), output.get_shape().as_list())

        with tf.variable_scope("global_avg_pooling"):
            output = self.batch_norm(output, self.list_filter[-1] * 4, self.is_training)
            output = tf.nn.relu(output)
            filter_size = output.get_shape()[-2]
            output = self.avg_pool(output, filter_size, filter_size)

        with tf.variable_scope("FC"):
            total_features = int(output.get_shape()[-1])
            output = tf.reshape(output, [-1, total_features])
            W = tf.get_variable('DW', [total_features, self.num_classes], tf.float32,
                                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.get_variable('bias', [self.num_classes], tf.float32, initializer=tf.constant_initializer(0.0))
            self.logits = tf.matmul(output, W) + b

        self.prediction = tf.nn.softmax(self.logits)
        self.correct_pred = tf.equal(tf.argmax(self.prediction, 1), tf.argmax(self.y_, 1))
        self.accuracy = tf.reduce_mean(tf.cast(self.correct_pred, tf.float32))

    def losses(self):
        self.l2_loss = tf.add_n([tf.nn.l2_loss(var) for var in tf.trainable_variables()])
        self.cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.logits, labels=self.y_))
        self.total_loss = self.weight_decay * self.l2_loss + self.cross_entropy

    def train_step(self):
        self.learning_rate = tf.placeholder(tf.float32)
        self.train_step = tf.train.MomentumOptimizer(learning_rate=self.learning_rate, momentum=0.9,
                                                     use_nesterov=True).minimize(self.total_loss,
                                                                                 global_step=self.global_step)

    def init_session(self):
        if not os.path.isdir(self.current_save_path):
            os.mkdir(self.current_save_path)
            os.mkdir(os.path.join(self.current_save_path, 'current'))

        if not os.path.isdir(self.logs_folder):
            os.mkdir(self.logs_folder)
        self.current_save_path = os.path.join(self.current_save_path, 'current') + '/'
        self.sess = tf.InteractiveSession()
        self.saver = tf.train.Saver(max_to_keep=10)
        if not os.path.isfile(self.current_save_path + 'model.ckpt.index'):
            print('Create new model')
            self.sess.run(tf.global_variables_initializer())
            print('OK')
        else:
            print('Restoring existed model')
            self.saver.restore(self.sess, self.current_save_path + 'model.ckpt')
            print('OK')
            print(self.global_step.eval())
        writer = tf.summary.FileWriter
        self.summary_writer = writer(self.logs_folder)

    def log_loss_accuracy(self, loss, accuracy, epoch, prefix):
        summary = tf.Summary(value=[
            tf.Summary.Value(
                tag='loss_%s' % prefix, simple_value=float(loss)),
            tf.Summary.Value(
                tag='accuracy_%s' % prefix, simple_value=float(accuracy))
        ])
        self.summary_writer.add_summary(summary, epoch)

    def train(self, num_epoch, batch_size=512):
        train_img = []
        train_label = []
        for i in range(len(self.train_data)):
            train_img.append(self.train_data[i][0])
            train_label.append(self.train_data[i][1])

        num_batch = int(len(train_img) // batch_size)
        current_epoch = int(self.global_step.eval() / num_batch)

        lnr = self.init_lnr

        for epoch in range(current_epoch + 1, num_epoch + 1):
            current_step = int(self.global_step.eval())
            print('Epoch:', str(epoch))
            np.random.shuffle(self.train_data)
            start_time = time.time()
            train_img = []
            train_label = []
            for i in range(len(self.train_data)):
                train_img.append(self.train_data[i][0])
                train_label.append(self.train_data[i][1])

            for i in range(len(self.reduce_lnr)):
                if self.reduce_lnr[i] <= epoch:
                    lnr = self.init_lnr * (0.1 ** (i + 1))

            print("Learning rate: %f" % lnr)
            sum_loss = []
            sum_l2 = []
            sum_acc = []
            for batch in range(num_batch):
                top = batch * batch_size
                bot = min((batch + 1) * batch_size, len(self.train_data))
                batch_img = np.asarray(train_img[top:bot])
                batch_label = np.asarray(train_label[top:bot])

                batch_img = Resnet_input.augmentation(batch_img, self.input_size)

                ttl, l2l, _, acc = self.sess.run([self.total_loss, self.l2_loss, self.train_step, self.accuracy],
                                                 feed_dict={self.x: batch_img, self.y_: batch_label,
                                                            self.is_training: True, self.learning_rate: lnr})
                print('Training on batch %s / %s.' % (str(batch + 1), str(num_batch)), end='\r')
                sum_loss.append(ttl)
                sum_acc.append(acc)
                sum_l2.append(l2l)
            time_per_epoch = time.time() - start_time
            mean_loss = np.mean(sum_loss)
            mean_acc = np.mean(sum_acc)
            mean_l2 = np.mean(sum_l2)
            print('\nTraining time: ', str(timedelta(seconds=time_per_epoch)))
            print('Training loss: %f' % mean_loss)
            print('L2 loss: %f' % mean_l2)
            print('Train accuracy: %f' % mean_acc)
            self.saver.save(self.sess, save_path=self.current_save_path + 'model' + str(epoch) + '.ckpt')
            self.saver.save(self.sess, save_path=self.current_save_path + 'model.ckpt')
            self.log_loss_accuracy(loss=mean_loss, accuracy=mean_acc, epoch=epoch, prefix='train')

            mean_loss, mean_acc = self.valid(batch_size=batch_size)
            self.log_loss_accuracy(loss=mean_loss, accuracy=mean_acc, epoch=epoch, prefix='valid')
            if mean_acc >= 0.9:
                self.saver.save(self.sess, save_path=self.valid_save_path + 'model' + str(round(mean_acc, 4)) + '.ckpt')

            if self.test_during_train:
                mean_loss, mean_acc, _, _ = self.test(batch_size=batch_size)
                self.log_loss_accuracy(loss=mean_loss, accuracy=mean_acc, epoch=epoch, prefix='test')

    def test(self, batch_size=1, save_file=None):
        if save_file is not None:
            self.sess.close()
            self.sess = tf.InteractiveSession()
            print("Restoring session from %s for testing" % save_file)
            self.saver.restore(self.sess, self.current_save_path + save_file)
            print("OK")
        all_score = []
        all_label = []
        test_img = []
        test_label = []
        for i in range(len(self.test_data)):
            test_img.append(self.test_data[i][0])
            test_label.append(self.test_data[i][1])

        num_batch = int(len(test_img) // batch_size)
        sum_loss = []
        sum_acc = []
        for batch in range(num_batch):
            top = batch * batch_size
            bot = min((batch + 1) * batch_size, len(self.test_data))
            batch_img = np.asarray(test_img[top:bot])
            batch_label = np.asarray(test_label[top:bot])
            batch_img = Resnet_input.augmentation(batch_img, self.input_size, testing=True)
            logits = self.sess.run([self.prediction],
                                   feed_dict={self.x: batch_img, self.y_: batch_label, self.is_training: False})
            logits = np.asarray(logits)
            for i in range(logits.shape[1]):
                all_score.append(logits[0][i])
                all_label.append(batch_label[i])

            ttl, l2l, acc = self.sess.run([self.total_loss, self.l2_loss, self.accuracy],
                                          feed_dict={self.x: batch_img, self.y_: batch_label,
                                                     self.is_training: False})
            print('Testing on batch %s / %s.' % (str(batch + 1), str(num_batch)), end='\r')
            sum_loss.append(ttl)
            sum_acc.append(acc)
        mean_loss = np.mean(sum_loss)
        mean_acc = np.mean(sum_acc)
        print('\nTest loss: %f' % mean_loss)
        print('Test accuracy: %f' % mean_acc)
        return mean_loss, mean_acc, all_score, all_label
    
    def valid(self, batch_size=1):
        valid_img = []
        valid_label = []
        for i in range(len(self.valid_data)):
            valid_img.append(self.valid_data[i][0])
            valid_label.append(self.valid_data[i][1])

        num_batch = int(len(valid_img) // batch_size)
        sum_loss = []
        sum_acc = []
        for batch in range(num_batch):
            top = batch * batch_size
            bot = min((batch + 1) * batch_size, len(self.valid_data))
            batch_img = np.asarray(valid_img[top:bot])
            batch_label = np.asarray(valid_label[top:bot])
            batch_img = Resnet_input.augmentation(batch_img, self.input_size, testing=True)
            ttl, l2l, acc = self.sess.run([self.total_loss, self.l2_loss, self.accuracy],
                                          feed_dict={self.x: batch_img, self.y_: batch_label,
                                                     self.is_training: False})
            print('Validating on batch %s / %s.' % (str(batch + 1), str(num_batch)), end='\r')
            sum_loss.append(ttl)
            sum_acc.append(acc)
        mean_loss = np.mean(sum_loss)
        mean_acc = np.mean(sum_acc)
        print('\nValid loss: %f' % mean_loss)
        print('Valid accuracy: %f' % mean_acc)
        return mean_loss, mean_acc